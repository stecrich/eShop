package archiveTest;

import cz.cvut.eshop.archive.ItemPurchaseArchiveEntry;
import cz.cvut.eshop.archive.PurchasesArchive;
import cz.cvut.eshop.shop.Item;
import cz.cvut.eshop.shop.Order;
import cz.cvut.eshop.shop.ShoppingCart;
import cz.cvut.eshop.shop.StandardItem;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;

public class PurchaseArchiveTest {

    PurchasesArchive testArchive;
    StandardItem sampleItem;
    StandardItem multipleItem;
    StandardItem notSoldItem;
    Order sampleNotArchivedOrder;

    @Before
    public void init(){
        sampleItem              = new StandardItem(1, "sampleItem", 10, "testItems",5);
        multipleItem            = new StandardItem(2, "multipleItem", 20, "testItems",15);
        notSoldItem             = new StandardItem(3, "notSoldYetItem", 50, "notSold",51);
        ArrayList<Item> items   = new ArrayList<>();
        ItemPurchaseArchiveEntry sampleItemEntry    = new ItemPurchaseArchiveEntry(sampleItem);
        ItemPurchaseArchiveEntry multipleItemEntry  = new ItemPurchaseArchiveEntry(multipleItem);
        ShoppingCart sampleCart;
        Order sampleOrder;

        ArrayList<Order> orderArchive = new ArrayList<>();
        HashMap<Integer, ItemPurchaseArchiveEntry> itemArchive = new HashMap<>();

        items.add(sampleItem);
        for (int i = 0; i < 5; i++) {
            items.add(multipleItem);
        }

        sampleCart  = new ShoppingCart(items);
        sampleOrder = new Order(sampleCart);
        orderArchive.add(sampleOrder);
        multipleItemEntry.increaseCountHowManyTimesHasBeenSold(4);

        itemArchive.put(1,sampleItemEntry);
        itemArchive.put(2,multipleItemEntry);

        testArchive = new PurchasesArchive(itemArchive, orderArchive);
    }

    @Test
    public void getHowManyTimesHasBeenItemSold_test() {
        Assert.assertEquals(5, testArchive.getHowManyTimesHasBeenItemSold(multipleItem));
    }

    @Test
    public void getHowManyTimesHasBeenItemSold_notSold_test() {
        Assert.assertEquals(0, testArchive.getHowManyTimesHasBeenItemSold(notSoldItem));
    }

    @Test
    public void putOrderToPurchasesArchive_test() {
        ArrayList<Item> items   = new ArrayList<>();
        items.add(notSoldItem);
        ShoppingCart sampleCart = new ShoppingCart(items);
        sampleNotArchivedOrder  = new Order(sampleCart);

        testArchive.putOrderToPurchasesArchive(sampleNotArchivedOrder);
        Assert.assertEquals(1,testArchive.getHowManyTimesHasBeenItemSold(notSoldItem));
    }
}
