package archiveTest;

import cz.cvut.eshop.archive.ItemPurchaseArchiveEntry;
import cz.cvut.eshop.shop.StandardItem;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ItemPurchaseArchiveEntryTest {

    StandardItem sampleItem;
    ItemPurchaseArchiveEntry testArchive;

    @Before
    public void init(){
        sampleItem  = new StandardItem(1,"SampleItem",10, "test",5);
        testArchive = new ItemPurchaseArchiveEntry(sampleItem);
    }

    @Test
    public void increaseCountHowManyTimesHasBeenSold_test() {
        testArchive.increaseCountHowManyTimesHasBeenSold(9); // Sold count already set to one during initial insert
        Assert.assertEquals(10, testArchive.getCountHowManyTimesHasBeenSold());
    }

    @Test
    public void getRefItem_test() {
        Assert.assertEquals(sampleItem, testArchive.getRefItem());
    }

}
